package task1;

/**
 * Created by Никита on 25.01.2016.
 */
public class TrafficLight {
    private Light light;

    enum Light {
        RED, GREEN, YELLOW
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

}
