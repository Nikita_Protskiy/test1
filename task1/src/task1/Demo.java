package task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Никита on 25.01.2016.
 */
public class Demo {

    public void writeMessage() {
        System.out.println("Введите номер минуты ");
    }

    public int putCountOfMinute() {
        int count = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            count = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int calculateNumberOfMinute(int putCountOfMinute) {
        return putCountOfMinute % 6;
    }

    public void showColorOfTrafficLight(int calculateNumberOfMinute) {
        TrafficLight trafficLight = new TrafficLight();
        switch (calculateNumberOfMinute) {
            case 0:
            case 1:
                trafficLight.setLight(TrafficLight.Light.RED);
                System.out.println("the Light is " + trafficLight.getLight());
                break;
            case 2:
                trafficLight.setLight(TrafficLight.Light.YELLOW);
                System.out.println("the Light is " + trafficLight.getLight());
                break;
            case 3:
            case 4:
            case 5:
                trafficLight.setLight(TrafficLight.Light.GREEN);
                System.out.println("the Light is " + trafficLight.getLight());
                break;
        }
    }
}
